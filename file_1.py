#!/usr/bin/env python3
# -*- coding: utf-8 -*-

first_char = input("Input the first symbol: ")

# Открываем файл на чтение
file_name = "file1.txt"
f = open(file_name, "r")

counter = 0
# чтение файла построчно
for line in f:
    # проверка первого символа
    if (first_char[0] == line[0]):
        counter += 1

f.close()

print("number of lines = ", counter)
