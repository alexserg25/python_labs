#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from random import randint

## Задаем размерность массива
n = 7

## Создание и заполнение массива нулями
print("Итоговый массив:")
mas = [[0] * n for i in range(n)]
for x in range(n):
    for y in range(n):
        mas[x][y] = randint(-50, 50)
    print(mas[x])

## Получаем сумму и количество положительных элементов над главной диагональю
print("Элементы над главной диагональю:")
sum = 0
count = 0
for pos in range(n - 1):
    print(mas[pos][pos + 1], ' ')
    if mas[pos][pos + 1] > 0:
        sum += mas[pos][pos + 1]
        count += 1
print("Сумма = ", sum, ", количество = ", count)
