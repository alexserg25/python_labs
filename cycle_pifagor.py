#!/usr/bin/env python3
# -*- coding: utf-8 -*-

k = int(input("Input \"K\": "))

# Цикл для m  (1<m<k)
for m in range(1, k):
    # Цикл для "n" (до "m", а не "k", иначе "a" будет < 1)
    for n in range(1, m):
        a = m**2 - n**2
        b = 2 * m * n
        c = m**2 + n**2

        if (c**2 == a**2 + b**2):
            print(m, n, a, b, c)
