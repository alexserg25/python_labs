#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## Задаем размерность массива
n = 7

## Создание и заполнение массива нулями
mas = [[0] * n for i in range(n)]

## Присвоение заданных значений диагоналям
for pos in range(n):
    mas[pos][pos] = pos + 1
    mas[pos][n - 1 - pos] = pos + 1

# Вывод как массива
for pos in range(n):
    print(mas[pos])

## Вывод с преобразованием в строку и символом ' ' в качестве разделителя
for pos in range(n):
    print(' '.join(map(str, mas[pos])))
