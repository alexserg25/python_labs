#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from random import randint

## Задаем длинну массива
n = 7


## Создание и заполнение массива
mas = [0] * n
for pos in range(n):
    mas[pos] = randint(-50, 50)
print("Оригинальный массив: ", mas)

## Получение четных элементов массива (Делаем срез с шагом 2 со 2-го элемента)
mas_not_sort = mas[1:n:2]

## Тоже получение четных элементов, но в цикле
#mas_not_sort = []
#for pos in range(1, n, 2):
#    mas_not_sort.append(mas[pos])

print("Неотсотированные элементы четных номеров: ", mas_not_sort)
## Сортировка массива в обратном порядке
mas_sort = sorted(mas_not_sort, reverse = True)
print("Отсортированные элементы четных номеров: ", mas_sort)

## Заменяем четные элементы на сортированные
i = 0
for pos in range(1, n, 2):
    mas[pos] = mas_sort[i]
    i += 1

print("Основной массив с отсортированными четными элементами: ", mas)
