#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from random import randint

file_name = "file2.txt"

# создание "file2.txt"
f = open(file_name, "w")
for pos in range(100):
    f.write(str(randint(0, 1000)) +  "\n")
f.close()

m = int(input("Input m: "))
n = int(input("Input n: "))

# Открываем файл на чтение
f = open(file_name, "r")
counter = 0
for line in f:
    # проверка на деление по модулю
    if (int(line) % m == 0 and int(line) % n != 0):
        counter += 1
        print(line)

f.close()

print("Number of lines = ", counter)
